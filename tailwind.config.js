/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    container: {
      // you can configure the container to be centered
      center: true,

      // or have default horizontal padding
      padding: {
        DEFAULT: "1rem",
        sm: "2rem",
        lg: "4rem",
        xl: "5rem",
        "2xl": "6rem",
      },
    },
    extend: {
      colors: {
        aimiGreen: "#3E8F49",
        aimiGreenButton: "#159500",
        aimiRed: "#EE4444",
        aimiGray: "#777777",
      },
    },
  },
  plugins: [],
}
