import { AgentCard } from "../Components/Card"
import { CompassOutlined, RightOutlined } from "@ant-design/icons"
import { Button, Collapse, Input, Modal, theme } from "antd"
import Head from "next/head"
import Link from "next/link"
import React, { useState } from "react"
import WAIllustration from "../assets/illustrationVidi.svg"
import Layout from "../layouts/Layout"

const { Search } = Input

const Index: React.FC = () => {
  // hooks
  const { token } = theme.useToken()
  const [open, setOpen] = useState(false)
  const [confirmLoading, setConfirmLoading] = useState(false)
  const [modalText, setModalText] = useState("Content of the modal")

  const showModal = () => {
    setOpen(true)
  }

  const panelStyle = {
    marginBottom: 24,
    background: token.colorFillAlter,
    borderRadius: token.borderRadiusLG,
    border: "none",
  }

  const onChange = (key: string | string[]) => {
    console.log(key)
  }

  const handleOk = () => {
    setModalText("The modal will be closed after two seconds")
    setConfirmLoading(true)
    setTimeout(() => {
      setOpen(false)
      setConfirmLoading(false)
    }, 2000)
  }

  const handleCancel = () => {
    console.log("Clicked cancel button")
    setOpen(false)
  }

  return (
    <Layout>
      <Head>
        <title>Flimty - Daftar Agent</title>
      </Head>
      <main>
        {/* search bar */}
        <section className="mb-4">
          <div className="mb-4">
            <h1 className="font-semibold">Masukkan Kota/Kecamatan</h1>
          </div>
          <Search
            prefix={<CompassOutlined className="text-aimiGreen" />}
            placeholder="Jakarta..."
            size="large"
            allowClear
          />
        </section>

        {/* accordion collapse */}
        <section>
          <div className="mb-4">
            <h1 className="font-semibold">Atau pilih dari list dibawah</h1>
          </div>
          <Collapse
            defaultActiveKey={"1"}
            bordered={false}
            style={{ background: token.colorBgContainer }}
            expandIconPosition="right"
            expandIcon={({ isActive }) => (
              <RightOutlined rotate={isActive ? 90 : 0} />
            )}
            onChange={onChange}
          >
            <Collapse.Panel style={panelStyle} header="Jakarta" key="1">
              <AgentCard onClick={showModal} />
              <AgentCard onClick={showModal} />
              <AgentCard onClick={showModal} />
            </Collapse.Panel>
          </Collapse>
        </section>
      </main>

      <Modal
        // title="Title"
        centered
        open={open}
        // onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        cancelButtonProps={{ style: { display: "none" } }}
        okButtonProps={{ style: { display: "none" } }}
      >
        <WAIllustration className="w-full h-72" />

        <div className="mt-8">
          <h1 className="font-semibold text-lg text-center">
            Pesan Melalui Agent Terdekat
          </h1>
          <p className="text-center text-gray-500">
            Yuk, masukin nomor Whatsapp kamu! Kita bisa arahin untuk order
            Flimty di agen terdekat loh!
          </p>
        </div>

        <Input.Group className="mt-8" compact>
          <Input style={{ width: "20%" }} defaultValue="+62" disabled />
          <Input style={{ width: "80%" }} defaultValue="" />
        </Input.Group>

        <Link href={"/thankyou"}>
          <Button
            className="bg-aimiGreenButton w-full mt-8 font-semibold"
            size="large"
            type="primary"
          >
            Order Sekarang
          </Button>
        </Link>
      </Modal>
    </Layout>
  )
}

export default Index
