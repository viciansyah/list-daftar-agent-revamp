import React from "react"
import Layout from "../../layouts/Layout"
import ThankYouFlimty from "../../assets/thankyouflimty.svg"
import { CheckOutlined } from "@ant-design/icons"
import Link from "next/link"
import { Button } from "antd"

const data = [
  "Cari lokasi agen terdekat",
  "Pilih produk yang ingin kamu pesan",
  "Lakukan pembayaran",
  "Pesanan diantar!",
]

const index = () => {
  return (
    <Layout>
      <main>
        <section className="flex flex-col items-center border-b-[1px] pb-8 mb-8">
          <ThankYouFlimty />
          <div className="text-center lg:w-2/3">
            <h1 className="text-xl font-semibold">
              Terima kasih telah berbelanja dengan kami.
            </h1>
            <br />
            <p className="text-sm font-light">
              Pesanan Kamu telah diterima dan akan segera diproses. Kami akan
              mengirimkan konfirmasi ke nomor handphone yang Kamu berikan.
            </p>
          </div>
        </section>

        <section className="flex flex-col mb-8">
          <p className="text-base font-semibold">Panduan Transaksi:</p>
          <br />

          <div className="border rounded-lg py-4">
            <ol>
              {data.map((value, index, array) => (
                <li key={index}>
                  <div
                    className={`flex justify-between items-center ${
                      index === array.length - 1 ? null : "border-b"
                    } p-4 mx-4`}
                  >
                    <div className="flex justify-between items-center">
                      <div className="bg-aimiGreen w-5 h-5 justify-center items-center text-center rounded-full text-white font-light text-sm">
                        {index + 1}
                      </div>
                      <span className="ml-4">{value}</span>
                    </div>
                    <CheckOutlined className="text-aimiGreen" />
                  </div>
                </li>
              ))}
            </ol>
          </div>

          <Link href={"/"}>
            <Button
              className="bg-aimiGreenButton w-full mt-8 font-semibold"
              size="large"
              type="primary"
            >
              Kembali ke Daftar Agen
            </Button>
          </Link>
        </section>
      </main>
    </Layout>
  )
}

export default index
