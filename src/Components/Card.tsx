import {
  BankOutlined,
  CheckCircleFilled,
  PhoneFilled,
  RocketFilled,
  SmileFilled,
} from "@ant-design/icons"
import { Button } from "antd"
import Image from "next/image"

export const AgentCard = ({ onClick }: { onClick: any }) => {
  return (
    <div className="w-full border-[1px] border-aimiGreen rounded-md bg-[#F2FAF4] p-4 flex mb-4">
      <div className="flex-1 flex-col">
        <div className="mb-4">
          <div className="flex items-center ">
            <BankOutlined className="mr-2" />
            <h1 className="font-bold">Agent 1</h1>
          </div>
          <p className="text-xs md:text">
            JL. PALEM RAYA 3 NO.4 DUSUN ABADI KEL. JEUMPUT AJUN, KEC. DARUL
            IMARAH JEUMPET AJUN DARUL IMARAH ACEH BESAR NANGGROE ACEH DARUSSALAM
          </p>
        </div>

        <div className="flex items-center">
          <div className="flex mr-8">
            <Image
              className="rounded-full mr-2 aspect-auto object-contain bg-white"
              width="24"
              height="24"
              src="/picture/Instagram.png"
              alt="Instagram"
            />
            <Image
              className="rounded-full mr-2 aspect-auto object-contain bg-white"
              width="24"
              height="24"
              src="/picture/Tokopedia.png"
              alt="Tokopedia"
            />
            <Image
              className="rounded-full mr-2 aspect-auto object-contain bg-white"
              width="24"
              height="24"
              src="/picture/Lazada.png"
              alt="Lazada"
            />
          </div>
          <Button
            onClick={onClick}
            className="bg-aimiGreenButton"
            size="large"
            type="primary"
          >
            <div className="flex items-center">
              <Image
                className="hidden lg:block rounded-full aspect-auto object-contain"
                width="32"
                height="32"
                src="/picture/Whatsapp.png"
                alt="Lazada"
              />
              <span className="font-bold lg:pr-4">Beli Disini</span>
            </div>
          </Button>
        </div>
      </div>
      <div className="lg:flex-1 flex justify-center items-center shrink">
        <div>
          <div className="flex items-center mb-2">
            <PhoneFilled rotate={90} className="mr-2 text-aimiGray" />
            <p>Whatsapp: +628155676751</p>
            <CheckCircleFilled className="md:ml-2 ml-0 text-aimiRed" />
          </div>
          <div className="flex items-center mb-2">
            <RocketFilled className="mr-2 text-aimiGray" />
            <p>Instant Courier</p>
            <CheckCircleFilled className="md:ml-2 ml-0 text-aimiRed" />
          </div>
          <div className="flex items-center mb-2">
            <SmileFilled className="mr-2 text-aimiGray" />
            <p>COD / Cash on Delivery</p>
            <CheckCircleFilled className="md:ml-2 ml-0 text-aimiRed" />
          </div>
        </div>
      </div>
    </div>
  )
}
