import React, { useState } from "react"
import styled from "styled-components"
import { Collapse as AntCollapse } from "antd"

const StyledCollapse = styled(AntCollapse.Panel)`
  &&& {
    border: none;
    border-radius: 0px;
    background-color: #f7f7f7;
    box-shadow: none;
  }
  .ant-collapse-content {
    background: palegreen;
  }
`

const CustomCollapse = (props) => {
  const [disabled, setDisabled] = useState(true)
  return (
    <AntCollapse onChange={() => setDisabled((prev) => !prev)}>
      <StyledCollapse
        {...props}
        header={props.header}
        key="1"
        showArrow={false}
        bordered={false}
        extra={
          <span>
            <span style={{ color: "#0076de", float: "right" }}>
              {disabled ? "SHOW" : "HIDE"}
            </span>
          </span>
        }
      >
        {props.children}
      </StyledCollapse>
    </AntCollapse>
  )
}

export default CustomCollapse
