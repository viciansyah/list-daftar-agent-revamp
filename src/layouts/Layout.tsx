import { ConfigProvider } from "antd"
import { ReactNode } from "react"

interface LayoutProps {
  children: ReactNode
}

const Layout = (props: LayoutProps) => {
  const { children } = props

  return (
    <ConfigProvider
      theme={{
        token: {
          colorPrimary: "#3E8F49",
        },
      }}
    >
      <div className="container mx-auto lg:px-64 md:px-32 px-4 pt-4">
        {children}
      </div>
    </ConfigProvider>
  )
}

export default Layout
